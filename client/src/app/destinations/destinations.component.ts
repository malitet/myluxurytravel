import { Component, OnInit } from '@angular/core';

import { DestinationsModel } from './destinations.model';

import { DestinationsService } from './destinations.service';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.css'],
  providers: [DestinationsService]
})
export class DestinationsComponent implements OnInit {

  data: DestinationsModel;

  errorMsg: string = '';
  loading: boolean = false;

  selectedDate: any;
  selectedTime: any;

  _showDestinations = true;
  _showCarOffers = false;
  locationLoading = false;

  constructor(private service: DestinationsService) {
    this.errorMsg = '';
    this.loading = false;
  }

  ngOnInit() {
  }

  // this.data can be filled with data binding, but for this experiment, I will include inside this function.
  getCarOfferByDestination(from: string, to: string, offerType: string, vehicleType: string) {
    if (!this.selectedDate || !this.selectedTime) {
      this.errorMsg = 'Please Select Date and Time';
      return;
    }
    this.loading = true;
    this.errorMsg = '';
    this.data = {
      "settings": {
        "vehicleType": vehicleType,
        "surcharge": 15
      },
      "myDriverData": {
        "originPlaceId": from,
        "destinationPlaceId": to,
        "selectedStartDate": this.getSelectedStartDate(),
        "type": offerType
      }
    }
    this.getCarOffersByType();
  }

  // Sends request to API and gets filtered cars
  getCarOffersByType() {
    this.service.getCarByType(this.data).subscribe(
      (data) => {
        this.data = data;
        this.loading = false;
        this._showCarOffers = true;
        this._showDestinations = false;
      },
      (error: any) => {
        this.loading = false;
        this.errorMsg = error.data.errorDetailsMessage;
      }
    );
  }

  // Returns time and date 
  getSelectedStartDate() {
    return this.selectedDate + 'T' + this.selectedTime + ':00' + this.getTimeZone();
  }

  // Get timezone
  getTimeZone() {
    let date = new Date();
    let timezone = date.getTimezoneOffset() / -60;
    let prefix = '';
    if (timezone >= 0) {
      prefix = '+'
    }
    return prefix + timezone + ':00';
  }

  // Clear error messages that may appear to hide alert
  clearAlerts() {
    this.errorMsg = '';
  }

  // Handle emits from child components
  handleEventEmits($event) {
    if ($event.source === 'goBack' && $event.component === 'carOffersComponent') {
      this._showCarOffers = false;
      this._showDestinations = true;
    }
  }

}
