// Application info and settings
exports.config = {
    app: {
        name: 'My Luxury Travel',
        version: '1.0',
        description: 'My Luxury Travel Server'
    },
    server: {
        port: 3000
    },
    myDriverApiUrl: 'https://www.mydriver.com/api/v2/'
};
