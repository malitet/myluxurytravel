# myLuxuryTravel - Programming exercise #

### What is this repository for? ###

* This repository provides solution for programming excercise from [myDriverProgrammingExercise.pdf](https://bitbucket.org/malitet/myluxurytravel/src/1e3777e8a2d7244507cd792ca0f33a892d1b1e31/myDriverProgrammingExercise.pdf?at=master&fileviewer=file-view-default)

### Technologies ###

* Node.js
* Express.js
* Angular2 CLI (https://github.com/angular/angular-cli)
* Bootstrap 

### Info ###
* Angular runs on http://localhost:4200
* Node.js server runs on http://localhost:3000

### Tested Environmnent ###

* Npm version: 4.2.0
* Node.js version: v6.9.5
* Express.js version: 4.14.1


### Clone Repository ###

* Create New Folder (folder name can be changed later) 
* Open terminal and type "git clone https://malitet@bitbucket.org/malitet/myluxurytravel.git"

### Setup Server ###

Server servers Angular CLI built files stored inside /server/public folder. If you want to make changes in front end, follow "Setup Client" part.

* Open /server directory, open terminal and type "npm install" command
* After npm finish installing modules, type "node server.js" to start the server or use "nodemon" command for monitoring node.js process
* Open browser and navigate to http://localhost:3000/



### Setup Client ###

Client holds front end Angular CLI files.

* Go to /client directory and open terminal
* Type "npm install"
* After npm finish installing modules, type "npm install bootstrap@4.0.0-alpha.5 --save" (bootstrap4 doesn't install from package.json, needs to be fixed)
* If you want to navigate to angular project in browser, type "ng serve" on terminal inside /client directory. Note: to retrieve data in front end, the server must be up and running.
* Open browser and navigate to http://localhost:4200/
* Whatever changes you make, those changes can be viewed on the provided URL in previous step.

* **NOTE: ** If you want your changes to be served in Server, you must deploy your Angular CLI project.
* Go to /client directory and open terminal
* Type "npm build". For production type "ng build --prod --aot"
* This command will create new folder "dist" inside /client directory.
* Copy content of /client/dist and paste inside /server/public directory.
* Open browser and navigate to localhost:3000 and the changes will appear.

### Developer ###

* Ermal Selimi