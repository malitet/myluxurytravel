// Import controllers
var carOffersController = require('./../controller/caroffers.js');


// Export module Router
exports.router = {
    // Setup Properties
    routerModule: null,

    // Initialize the properties
    initialize: function (app) {
        if (!app)
            console.log('app argument is missing');

        // Set Express Framework Router  to routerModule
        this.routerModule = app.express.Router();
        // Make root path to serve static files from public folder
        this.routerModule.use('/', app.express.static('public'));
        // Set API Routes
        this.setRoutes();
    },
    setRoutes: function () {
        // Set headers for API
        this.routerModule.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

        // Declare post route for carOffersController
        this.routerModule.post('/api/getOfferByCarType', carOffersController.getOfferByCarType);
    }
};