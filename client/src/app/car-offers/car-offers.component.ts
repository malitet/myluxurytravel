import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-car-offers',
  templateUrl: './car-offers.component.html',
  styleUrls: ['./car-offers.component.css']
})
export class CarOffersComponent implements OnInit {

  @Input() data;
  @Output() emitCarOffersData: EventEmitter<any> = new EventEmitter<any>();

  loading: boolean = false;
  errorMessage: string = '';

  constructor() {
  }

  ngOnInit() {
  }

  // Format Final Amount gross for view
  formatFinalAmountGross(amount) {
    return (amount / 100).toFixed(2);
  }

  // Emits object to parent component to go back
  goBack() {
    this.emitCarOffersData.emit({
      source: 'goBack',
      component: 'carOffersComponent'
    });
  }

  checkOut() {
    alert('Comming Soon');
  }

}
