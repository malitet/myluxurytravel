function respondSuccess(res, data) {
    res.status(200).send({
        success: true,
        message: 'Success',
        data: data
    });
};

function respondError(res, data) {
    res.status(417).send({
        success: false,
        message: 'Fail',
        data: data
    });
};

function respondBadRequest(res, data) {
    res.status(400).send({
        success: false,
        message: 'Bad Request',
        data: data
    });
};

function resondUnauthorized(res, data) {
    res.status(401).send({
        success: false,
        message: 'Unauthorized',
        data: data
    });
}

exports.respondSuccess = respondSuccess;
exports.respondError = respondError;
exports.respondBadRequest = respondBadRequest;
exports.resondUnauthorized = resondUnauthorized;