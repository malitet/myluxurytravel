export class DestinationsModel {
    public settings: SettingsModel;
    public myDriverData: myDriverDataModel
}

export class SettingsModel {
    public vehicleType: string = 'FIRST_CLASS';
    public surcharge: number = 15;
}

export class myDriverDataModel {
    public originPlaceId: string = '';
    public destinationPlaceId: string = '';
    public selectedStartDate: string = '';
    public type: string = '';
}