import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class DestinationsService {

    private getOfferByCarTypeUrl = 'http://' + window.location.hostname + ':3000/api/getOfferByCarType';

    private headers: Headers;

    constructor(private http: Http) {
        this.headers = new Headers();
    }

    getCarByType(data) {
        return this.http.post(this.getOfferByCarTypeUrl, data, {
            headers: this.headers
        }).map(this.extractData).catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    private handleError(error: any) {
        return Observable.throw(error.json() || 'Server error');
    }
}
