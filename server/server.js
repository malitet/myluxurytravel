var application = {
    // Setup Properties
    config: require('./config/config.js').config,
    express: require('express'),
    path: require('path'),
    bodyParser: require('body-parser'),
    app: null,
    router: require('./config/router.js').router,
    server: null,
    // Initialize the properties
    initialize: function () {
        // Set express framework to property app
        this.app = this.express();

        // {arse application/x-www-form-urlencoded
        this.app.use(this.bodyParser.urlencoded({ extended: false }));

        // Parse application/json
        this.app.use(this.bodyParser.json());
        this.app.set('json spaces', 2);

        // Initialize Controllers and set Routes
        this.router.initialize(this);
        this.app.use('/', this.router.routerModule);

        // Start server
        this.startServer();
    },
    // Setup Server
    startServer: function () {
        this.server = this.app.listen(this.config.server.port, function () {
            console.log("Server started on: ", this.config.server.port);
        }.bind(this));
    }
}

// Start App Initializing
application.initialize();