webpackJsonp([1,5],{

/***/ 385:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 385;


/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(478);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(516);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(510);




if (__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/main.js.map

/***/ }),

/***/ 509:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(682),
            styles: [__webpack_require__(676)]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/app.component.js.map

/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__nav_nav_component__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__footer_footer_component__ = __webpack_require__(514);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__destinations_destinations_component__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__car_offers_car_offers_component__ = __webpack_require__(511);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










// Define the routes
var appRoutes = [];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__nav_nav_component__["a" /* NavComponent */],
                __WEBPACK_IMPORTED_MODULE_7__footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_8__destinations_destinations_component__["a" /* DestinationsComponent */],
                __WEBPACK_IMPORTED_MODULE_9__car_offers_car_offers_component__["a" /* CarOffersComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forRoot(appRoutes)
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/app.module.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarOffersComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CarOffersComponent = (function () {
    function CarOffersComponent() {
        this.emitCarOffersData = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.loading = false;
        this.errorMessage = '';
    }
    CarOffersComponent.prototype.ngOnInit = function () {
    };
    // Format Final Amount gross for view
    CarOffersComponent.prototype.formatFinalAmountGross = function (amount) {
        return (amount / 100).toFixed(2);
    };
    // Emits object to parent component to go back
    CarOffersComponent.prototype.goBack = function () {
        this.emitCarOffersData.emit({
            source: 'goBack',
            component: 'carOffersComponent'
        });
    };
    CarOffersComponent.prototype.checkOut = function () {
        alert('Comming Soon');
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(), 
        __metadata('design:type', Object)
    ], CarOffersComponent.prototype, "data", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]) === 'function' && _a) || Object)
    ], CarOffersComponent.prototype, "emitCarOffersData", void 0);
    CarOffersComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-car-offers',
            template: __webpack_require__(683),
            styles: [__webpack_require__(677)]
        }), 
        __metadata('design:paramtypes', [])
    ], CarOffersComponent);
    return CarOffersComponent;
    var _a;
}());
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/car-offers.component.js.map

/***/ }),

/***/ 512:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__destinations_service__ = __webpack_require__(513);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DestinationsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DestinationsComponent = (function () {
    function DestinationsComponent(service) {
        this.service = service;
        this.errorMsg = '';
        this.loading = false;
        this._showDestinations = true;
        this._showCarOffers = false;
        this.locationLoading = false;
        this.errorMsg = '';
        this.loading = false;
    }
    DestinationsComponent.prototype.ngOnInit = function () {
    };
    // this.data can be filled with data binding, but for this experiment, I will include inside this function.
    DestinationsComponent.prototype.getCarOfferByDestination = function (from, to, offerType, vehicleType) {
        if (!this.selectedDate || !this.selectedTime) {
            this.errorMsg = 'Please Select Date and Time';
            return;
        }
        this.loading = true;
        this.errorMsg = '';
        this.data = {
            "settings": {
                "vehicleType": vehicleType,
                "surcharge": 15
            },
            "myDriverData": {
                "originPlaceId": from,
                "destinationPlaceId": to,
                "selectedStartDate": this.getSelectedStartDate(),
                "type": offerType
            }
        };
        this.getCarOffersByType();
    };
    // Sends request to API and gets filtered cars
    DestinationsComponent.prototype.getCarOffersByType = function () {
        var _this = this;
        this.service.getCarByType(this.data).subscribe(function (data) {
            _this.data = data;
            _this.loading = false;
            _this._showCarOffers = true;
            _this._showDestinations = false;
        }, function (error) {
            _this.loading = false;
            _this.errorMsg = error.data.errorDetailsMessage;
        });
    };
    // Returns time and date 
    DestinationsComponent.prototype.getSelectedStartDate = function () {
        return this.selectedDate + 'T' + this.selectedTime + ':00' + this.getTimeZone();
    };
    // Get timezone
    DestinationsComponent.prototype.getTimeZone = function () {
        var date = new Date();
        var timezone = date.getTimezoneOffset() / -60;
        var prefix = '';
        if (timezone >= 0) {
            prefix = '+';
        }
        return prefix + timezone + ':00';
    };
    // Clear error messages that may appear to hide alert
    DestinationsComponent.prototype.clearAlerts = function () {
        this.errorMsg = '';
    };
    // Handle emits from child components
    DestinationsComponent.prototype.handleEventEmits = function ($event) {
        if ($event.source === 'goBack' && $event.component === 'carOffersComponent') {
            this._showCarOffers = false;
            this._showDestinations = true;
        }
    };
    DestinationsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-destinations',
            template: __webpack_require__(684),
            styles: [__webpack_require__(678)],
            providers: [__WEBPACK_IMPORTED_MODULE_1__destinations_service__["a" /* DestinationsService */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__destinations_service__["a" /* DestinationsService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__destinations_service__["a" /* DestinationsService */]) === 'function' && _a) || Object])
    ], DestinationsComponent);
    return DestinationsComponent;
    var _a;
}());
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/destinations.component.js.map

/***/ }),

/***/ 513:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(691);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__(690);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DestinationsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DestinationsService = (function () {
    function DestinationsService(http) {
        this.http = http;
        this.getOfferByCarTypeUrl = 'http://' + window.location.hostname + ':3000/api/getOfferByCarType';
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
    }
    DestinationsService.prototype.getCarByType = function (data) {
        return this.http.post(this.getOfferByCarTypeUrl, data, {
            headers: this.headers
        }).map(this.extractData).catch(this.handleError);
    };
    DestinationsService.prototype.extractData = function (res) {
        var body = res.json();
        return body;
    };
    DestinationsService.prototype.handleError = function (error) {
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json() || 'Server error');
    };
    DestinationsService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === 'function' && _a) || Object])
    ], DestinationsService);
    return DestinationsService;
    var _a;
}());
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/destinations.service.js.map

/***/ }),

/***/ 514:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-footer',
            template: __webpack_require__(685),
            styles: [__webpack_require__(679)]
        }), 
        __metadata('design:paramtypes', [])
    ], FooterComponent);
    return FooterComponent;
}());
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/footer.component.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavComponent = (function () {
    function NavComponent() {
    }
    NavComponent.prototype.ngOnInit = function () {
    };
    NavComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-nav',
            template: __webpack_require__(686),
            styles: [__webpack_require__(680)]
        }), 
        __metadata('design:paramtypes', [])
    ], NavComponent);
    return NavComponent;
}());
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/nav.component.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=C:/Users/malit/Desktop/myluxurytravel/client/src/environment.js.map

/***/ }),

/***/ 676:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 677:
/***/ (function(module, exports) {

module.exports = ".card img {\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    display: block;\r\n}"

/***/ }),

/***/ 678:
/***/ (function(module, exports) {

module.exports = ".profile-card {\r\n    background-color: #222222;\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.profile-pic {\r\n    border-radius: 50%;\r\n    position: absolute;\r\n    top: -65px;\r\n    left: 0;\r\n    right: 0;\r\n    margin: auto;\r\n    z-index: 1;\r\n    max-width: 100px;\r\n    -webkit-transition: all 0.4s;\r\n    transition: all 0.4s;\r\n}\r\n\r\n.profile-info {\r\n    color: #BDBDBD;\r\n    padding: 25px;\r\n    position: relative;\r\n    margin-top: 15px;\r\n}\r\n\r\n.profile-info h2 {\r\n    color: #E8E8E8;\r\n    letter-spacing: 4px;\r\n    padding-bottom: 12px;\r\n}\r\n\r\n.profile-info span {\r\n    display: block;\r\n    font-size: 12px;\r\n    color: #4CB493;\r\n    letter-spacing: 2px;\r\n}\r\n\r\n.profile-info a {\r\n    color: #4CB493;\r\n}\r\n\r\n.profile-info i {\r\n    padding: 15px 35px 0px 35px;\r\n}\r\n\r\n.profile-card:hover .profile-pic {\r\n    -webkit-transform: scale(1.1);\r\n            transform: scale(1.1);\r\n}\r\n\r\n.profile-card:hover .profile-info hr {\r\n    opacity: 1;\r\n}\r\n\r\n\r\n/* Underline From Center */\r\n\r\n.hvr-underline-from-center {\r\n    display: inline-block;\r\n    vertical-align: middle;\r\n    -webkit-transform: translateZ(0);\r\n    transform: translateZ(0);\r\n    box-shadow: 0 0 1px rgba(0, 0, 0, 0);\r\n    -webkit-backface-visibility: hidden;\r\n    backface-visibility: hidden;\r\n    -moz-osx-font-smoothing: grayscale;\r\n    position: relative;\r\n    overflow: hidden;\r\n}\r\n\r\n.hvr-underline-from-center:before {\r\n    content: \"\";\r\n    position: absolute;\r\n    z-index: -1;\r\n    left: 52%;\r\n    right: 52%;\r\n    bottom: 0;\r\n    background: #FFFFFF;\r\n    border-radius: 50%;\r\n    height: 3px;\r\n    -webkit-transition-property: all;\r\n    transition-property: all;\r\n    -webkit-transition-duration: 0.2s;\r\n    transition-duration: 0.2s;\r\n    -webkit-transition-timing-function: ease-out;\r\n    transition-timing-function: ease-out;\r\n}\r\n\r\n.profile-card:hover .hvr-underline-from-center:before, .profile-card:focus .hvr-underline-from-center:before, .profile-card:active .hvr-underline-from-center:before {\r\n    left: 0;\r\n    right: 0;\r\n    height: 1px;\r\n    background: #CECECE;\r\n}\r\n\r\n.my-btn {\r\n    background-color: #4CB493;\r\n    border: 0px;\r\n    color: #FFFFFF;\r\n    padding: 10px;\r\n    margin-top: 10px;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    display: block;\r\n}\r\n\r\n.my-btn:hover {\r\n    cursor: pointer;\r\n    background-color: #1cb383;\r\n}\r\n\r\n.date-time-wrapper {\r\n    margin-top: 10px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.date-time-wrapper input {\r\n    border: 0px;\r\n}"

/***/ }),

/***/ 679:
/***/ (function(module, exports) {

module.exports = ".footer {\r\n    position: absolute;\r\n    bottom: 0;\r\n    width: 100%;\r\n    height: 60px;\r\n    line-height: 60px;\r\n    background-color: #f5f5f5;\r\n    text-align: center;\r\n}"

/***/ }),

/***/ 680:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 682:
/***/ (function(module, exports) {

module.exports = "<app-nav></app-nav>\r\n<div class=\"container\">\r\n  <div class=\"starter-template\">\r\n    <app-destinations></app-destinations>\r\n    <router-outlet></router-outlet>\r\n  </div>\r\n</div>\r\n<app-footer></app-footer>"

/***/ }),

/***/ 683:
/***/ (function(module, exports) {

module.exports = "<section *ngIf=\"data\">\r\n  <div class=\"card col-lg-4\" *ngFor=\"let car of data.data\">\r\n    <img class=\"card-img-top\" src=\"https://www.mydriver.com{{ car.vehicleType.images.web }}\">\r\n    <div class=\"card-block\">\r\n      <h4 class=\"card-title\">{{car.vehicleType.title}}</h4>\r\n    </div>\r\n    <ul class=\"list-group list-group-flush\">\r\n      <li class=\"list-group-item\">Nr of passengers: {{car.vehicleType.nrOfPassengers}}</li>\r\n      <li class=\"list-group-item\">Nr of baggage: {{car.vehicleType.nrOfBaggage}}</li>\r\n      <li class=\"list-group-item\">{{car.vehicleType.exampleCar}}</li>\r\n    </ul>\r\n    <div class=\"alert alert-success fade in\" *ngIf=\"errorMsg !== ''\">\r\n      <strong>{{ car.currency }} {{formatFinalAmountGross(car.finalAmountGross)}}</strong>\r\n    </div>\r\n    <div class=\"card-block\">\r\n      <a href=\"#\" class=\"card-link\" (click)=\"goBack()\">Back</a>\r\n      <a href=\"#\" class=\"card-link\" (click)=\"checkOut()\">Check out</a>\r\n    </div>\r\n  </div>\r\n</section>"

/***/ }),

/***/ 684:
/***/ (function(module, exports) {

module.exports = "<section *ngIf=\"_showDestinations\">\r\n    <div class=\"alert alert-danger fade in\" *ngIf=\"errorMsg !== ''\">\r\n        <a href=\"#\" class=\"close\" data-dismiss=\"alert\" (click)=\"clearAlerts()\" aria-label=\"close\">&times;</a>\r\n        <strong>Error!</strong> {{ errorMsg }}\r\n    </div>\r\n    <img class=\"loader\" src=\"./../assets/img/loader.gif\" *ngIf=\"loading\" />\r\n    <div class=\"col-md-12\">\r\n        <h2>Select your destination</h2>\r\n    </div>\r\n    <div>\r\n        <div class=\"col-md-4\">\r\n            <div class=\"profile-card text-center\">\r\n                <div class=\"profile-info\">\r\n                    <h2 class=\"hvr-underline-from-center\">Paris<span>Saint-Martin, 75010 Paris to Airport</span></h2>\r\n                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum\r\n                        dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor\r\n                        sit amet, consectetur adipiscing elit.\r\n                    </div>\r\n                    <div class=\"date-time-wrapper\">\r\n                        <input type=\"date\" disabled>\r\n                        <input type=\"time\" disabled>\r\n                    </div>\r\n                    <button class=\"my-btn\">Not Available</button>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-4\">\r\n            <div class=\"profile-card text-center\">\r\n                <div class=\"profile-info\">\r\n\r\n                    <h2 class=\"hvr-underline-from-center\">Munich<span>Munich Karlsplatz \"Stachus\" to Airport</span></h2>\r\n                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum\r\n                        dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor\r\n                        sit amet, consectetur adipiscing elit.\r\n                    </div>\r\n                    <div class=\"date-time-wrapper\">\r\n                        <input type=\"date\" [(ngModel)]=\"selectedDate\">\r\n                        <input type=\"time\" [(ngModel)]=\"selectedTime\">\r\n                    </div>\r\n                    <button class=\"my-btn\" (click)=\"getCarOfferByDestination('ChIJh09k5PZ1nkcRXFNY-nVP0So', 'ChIJRW3aI1kTnkcRS89WOoV6xeM', 'DISTANCE', 'FIRST_CLASS')\">Book FirstClass Car</button>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-4\">\r\n            <div class=\"profile-card text-center\">\r\n\r\n                <div class=\"profile-info\">\r\n\r\n                    <h2 class=\"hvr-underline-from-center\">London<span>Marylebone, London to London Airport</span></h2>\r\n                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum\r\n                        dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor\r\n                        sit amet, consectetur adipiscing elit.\r\n                    </div>\r\n                    <div class=\"date-time-wrapper\">\r\n                        <input type=\"date\" disabled>\r\n                        <input type=\"time\" disabled>\r\n                    </div>\r\n                    <button class=\"my-btn\">Not Available</button>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<app-car-offers [data]=\"data\" *ngIf=\"_showCarOffers\" (emitCarOffersData)=\"handleEventEmits($event)\"></app-car-offers>"

/***/ }),

/***/ 685:
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer\">\r\n  <div class=\"container\">\r\n    <span class=\"text-muted\">\r\n      Copyright &#169; <a routerLink=\"/\">MyLuxuryTravel.com</a> | <a routerLink=\"/destinations\">Destinations</a>\r\n    </span>\r\n  </div>\r\n</footer>"

/***/ }),

/***/ 686:
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-fixed-top navbar-dark bg-inverse\">\r\n  <a class=\"navbar-brand\" href=\"#\">My Luxury Travel</a>\r\n  <ul class=\"nav navbar-nav\">\r\n    <li class=\"nav-item\" routerLinkActive=\"active\">\r\n      <a class=\"nav-link\" routerLink=\"/destinations\">Destinations <span class=\"sr-only\">(current)</span></a>\r\n    </li>\r\n    <!--<li class=\"nav-item\" routerLinkActive=\"active\">\r\n      <a class=\"nav-link\">Contact</a>\r\n    </li>-->\r\n  </ul>\r\n</nav>"

/***/ }),

/***/ 712:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(386);


/***/ })

},[712]);
//# sourceMappingURL=main.bundle.map