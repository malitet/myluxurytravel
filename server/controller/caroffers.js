var request = require('request'); // Load request module for external http request
var utilities = require('./../utilities/utilities.js'); // Load utilities for handling ex. responses
var myDriverApiUrl = require('./../config/config.js').config.myDriverApiUrl; // Load external api url

// Get all cars from myDriverApi and returns filtered cars by vehicleType
exports.getOfferByCarType = function (req, res) {
    request.post({
        headers: { 'content-type': 'application/json' },
        url: myDriverApiUrl + 'offers',
        body: JSON.stringify(req.body.myDriverData)
    }, function (error, response, data) {
        if (response.statusCode == 200) {
            utilities.respondSuccess(res, filterOffers(JSON.parse(data), req.body.settings));
        } else {
            utilities.respondError(res, JSON.parse(data));
        }
    });
}

// Filter cars with provided settings
function filterOffers(offers, settings) {
    return offers.filter(
        offer => offer.vehicleType.name === settings.vehicleType
    ).map(function (offer) {
        offer.finalAmountGross = calculateSurcharge(offer.finalAmountGross, settings.surcharge);
        return offer;
    });
}

// Calculate surcharge percentage of finalAmountGross
function calculateSurcharge(amount, percentage) {
    return parseInt(amount) + ((parseInt(amount) / 100) * percentage);
}

